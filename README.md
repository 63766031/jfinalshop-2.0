****Welcome to the jfinalshop-2.0RELEASE****
---------------------------------------------
> 本项目是以学习JFinal为目的把基于SSH开发的shopxx 2010开源版改成了JFinal2.0，以提高对JFinal的认识，也同样希望你能从jfinalshop项目中获得技术、思路上的提升，或者利用已有的代码减少您项目的开发周期，也欢迎志同道合的你参与到项目中来学习交流。

> **Note:** 
 - JDK版本：JDK1.6
 - Eclipse版本 ：Java EE IDE Luna Service Release 2 (4.4.2)
 - 开发框架：JFinal-2.0
 - 插件：JFinal-ext-3.12
 - 数据库：MySQL-5.6
 - 缓存：Ehcache-2.6.9
 - 模板：Beetl-2.2.3
 - 权限控制：Shiro-1.2.3

2.0功能列表
---------------------------------------------------------------------------------------
商品管理 | 订单管理 | 内容管理 | 模板管理 | 管理员 | 网站设置
------- | ------- | ------- | ------- | ------- | -------| -------
商品列表 | 订单列表 |内容管理 | 动态模板管理 | 角色管理 |系统设置
添加商品 | 收款单 | 文章管理 | 静态模板管理 | 资源管理 |支付方式
分类列表 | 退款单 | 文章分类 | 邮件模板管理 |收件箱 |配送方式
添加分类 | 发货单 | 导航管理 | 一键网站更新 |发件箱 |地区管理
类型列表 | 退货单 | 友情链接 | 文章更新 | 查看日志|物流公司
属性列表 | 会员注册协议 | 网页底部信息 | 日志配置 |
品牌列表 |  			| 商品更新 | |

----------
****Welcome to the jfinalshop-3.0RELEASE****
---------------------------------------------
> 本项目是基于jfinalshop2.0开发，JFinal2.0改成了JFinal2.2，这个版本中可以自动生成model，开发效率提升很多，目前代码整理完毕，3.0采用捐赠或成为项目成员的方式获取最新代码。


> 捐赠 ￥50 或以上 
----------------------------------------
 -可获得完整代码
 -涵盖全部功能

> 捐赠 ￥100 或以上 
----------------------------------------
 -成为开发者
 -随时获取最新代码

> 捐赠 ￥200 或以上 
----------------------------------------
 -提供必要的技术支持
 -专属内部开发QQ群，大神云集
 -优先获得新版本

> 重点说明：捐赠后，请用OSC私信或扣扣:187048359与群主联系，我们会在第一时间验证。你就可以获得相应的权限。
----------------------------------------

> **Note:** 
 - JDK版本：JDK1.7
 - Eclipse版本 ：Java EE IDE Luna Service Release 2 (4.4.2)
 - 代码管理： maven
 - 开发框架：JFinal-2.2
 - 插件：JFinal-ext2-2.0.5
 - 数据库：MySQL-5.7
 - 缓存：Ehcache-2.10.2
 - 模板：freemarker-2.3.23 (以前是beetl)
 - 权限控制：Shiro-1.3.0
 
 > **Tip:**
- 演示地址： http://www.jfinalshop.com/
- 后台地址： http://www.jfinalshop.com/admin
- 后台用户：admin 
- 后台密码：123456


网友捐赠  
---------------------------------------------------------------------  
网名     | 类型| 金额 | 日期   | 留言
-------- | --- | --- | --- |---
炳锋| 支付宝捐赠 | ￥200 | 2016-11-07 15:50 |
A 小翼哥| 微信捐赠 | ￥100 | 2016-11-05 20:46 | 支持开源，期盼合作
小辉| 支付宝捐赠 | ￥100 | 2016-11-03 16:02 |
齐淼| 微信捐赠 | ￥100 | 2016-11-02 19:24 |
闫江维| 支付宝捐赠 | ￥50 | 2016-11-01 19:40 |
宋哥| 支付宝捐赠 | ￥200 | 2016-10-31 22:37 | 捐款人宋立峰
乌合| 支付宝捐赠 | ￥100 | 2016-10-31 21:07 |
练习| 微信捐赠 | ￥100 | 2016-10-31 16:52 |
遨游| 支付宝捐赠 | ￥50 | 2016-10-29 09:04 |
郑晓川 | 支付宝捐赠 | ￥30 | 2016-10-28 11:05 |
Shadow | 支付宝捐赠 | ￥50 | 2016-10-28 09:51 |
王军 | 支付宝捐赠 | ￥50 | 2016-10-27 09:05 | 
春春 | 支付宝捐赠 | ￥30 | 2016-10-25 15:38 | 
岁月如刀 | 支付宝捐赠 | ￥100 | 2016-10-25 09:33 | 支持一下
朝明 | 支付宝捐赠 | ￥20 | 2016-10-24 08:37 | 
杨法涛 | 微信捐赠 | ￥50 | 2016-10-23 18:26 | 
郑楼鑫 | 支付宝捐赠 | ￥50 | 2016-10-22 20:54 | 
展翅 | 支付宝捐赠 | ￥20 | 2016-10-22 15:00 |  
燕少 | 支付宝捐赠 | ￥100 | 2016-10-20 15:27 |  
﹏紀先生 | 微信捐赠 | ￥100 | 2016-10-18 21:41 |  
芒果 | 支付宝捐赠 | ￥100 | 2016-10-15 15:31 |  越来越好！！
亮 | 支付宝捐赠 | ￥100 | 2016-10-14 12:08 |  
流云 | 支付宝捐赠 | ￥99 | 2016-10-12 12:17 |  祝兄弟开源事业，长长久久
S.L | 支付宝捐赠 | ￥100 | 2016-09-25 09:56 | 捐赠jfinalshop 
陕西微邻信息科技有限公司 | 支付宝捐赠 | ￥100 | 2016-05-22 11:38 |  加油
海娇 | 支付宝捐赠 | ￥100 | 2016-04-02 09:01 | 
丽华 | 支付宝捐赠 | ￥100 | 2016-03-22 07:56  |
wiliam | 支付宝捐赠 | ￥20 | 2015-10-30 11:31 | 希望jfinalshop越来越好
Ray | 支付宝捐赠 | ￥50 | 2015-10-19 22:36 | 作者辛苦，第一笔捐赠，加油！ 
花果山水帘洞齐天大圣 | 支付宝捐赠 | ￥50 | 2015-10-19 22:26 |  
汉林 | 支付宝捐赠 | ￥100 | 2015-10-09 16:03 | 我是昨晚建议捐赠的！加油哦！一点小意思 
昭强 | 支付宝捐赠 | ￥300 | 2015-10-08 21:34 | 今支持国产开源，小红加油  
佐佑 | 支付宝捐赠 | ￥50 | 2015-10-08 21:29 | 今天才关注这个项目，加油！
----------------------------------------------------
